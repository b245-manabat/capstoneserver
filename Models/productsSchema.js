const mongoose = require("mongoose");

const productsSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Input your product's name."]
	},
	image : {
		type : String,
		default : "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png"
	},
	category : {
		type : String,
		required : [true, "Please add a category!"]
	},
	description : {
		type : String,
		required : [true, "Add description to define your product."]
	},
	price : {
		type : Number,
		required : [true, "How much is your product?"]
	},
	stocks : {
		type : Number,
		required : [true, "How many stocks do you have?"]
	},
	itemDiscount : {
		type : Number,
		default : 0
	},
	itemsSold : {
		type : Number,
		default : 0
	},
	createdOn : {
		type : Date,
		default : new Date()
	}
})

module.exports = mongoose.model("Product", productsSchema);

