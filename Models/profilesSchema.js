const mongoose = require("mongoose");

const profilesSchema = new mongoose.Schema({
	userId: {
		type : String,
		required : [true, "Please login your account."]
	},
	firstName: {
		type : String,
		required : [true, "Please input your first name."]
	},
	lastName: {
		type : String,
		required : [true, "Please input your last name."]
	},
	address : {
		type : String,
		default : null
	}
})

module.exports = mongoose.model("Profile", profilesSchema);
