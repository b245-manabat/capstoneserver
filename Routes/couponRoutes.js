const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const couponController = require("../Controllers/couponController");


router.post("/create", auth.verify, couponController.addCoupon);
router.put("/edit/:code", auth.verify, couponController.updateCoupon);
router.put("/archive", auth.verify, couponController.archiveCoupon);
router.get("/view", auth.verify, couponController.viewCoupons);
router.get("/check", couponController.viewCoupon);

module.exports = router;