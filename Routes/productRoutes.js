const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productController = require("../Controllers/productController");

router.post("/add", auth.verify, productController.addProduct);
router.get("/viewall", productController.viewProducts);
router.get("/mens", productController.viewMens);
router.get("/womens", productController.viewWomens);
router.get("/toddlers", productController.viewToddlers);
router.get("/appliances", productController.viewAppliances);
router.get("/tools", productController.viewTools);
router.get("/others", productController.viewOthers);
router.get("/viewarchived", productController.archivedProducts);
router.get("/:id", productController.viewItem);
router.put("/update/:id", auth.verify, productController.updateProduct);
router.put("/archive/:id", auth.verify, productController.archiveProduct);

module.exports = router;