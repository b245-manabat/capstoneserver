const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

/*
	USER AREA
*/

module.exports.userRegistration = (request, response) => {
	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if(result !== null){
			return response.send(false)
		}else{
			let newUser = new User({
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
			})

			newUser.save()
			.then(save => response.send(save))
			.catch(error => response.send(false))
		}
	})
	.catch(error => response.send(false))
}

module.exports.userAuthentication = (request, response) => {
	let input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if(result === null){
			return response.send(false)
		}else{
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)});
			}else{
				return response.send(false)
			}
		}
	})
	.catch(error => response.send(false))
}

module.exports.userDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	User.findById(userData._id)
	.then(result => response.send({...result._doc,password: "confidential"}))
	.catch(error => response.send(false))
}

module.exports.nonAdmin = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		User.find({isAdmin: false})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.admin = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		User.find({isAdmin: true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.userAdmin = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	let user = request.body;

	if(userData.isAdmin&&userData.email!==user.email) {
		User.updateOne({email: user.email, isAdmin: false},{isAdmin: true},{new:true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.userNonAdmin = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	let user = request.body;

	if(userData.isAdmin&&userData.email!==user.email) {
		User.updateOne({email: user.email, isAdmin: true},{isAdmin: false},{new:true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.changePassword = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const user = userData.email;
	let oldPass = request.body.oldPassword;
	let newPass = request.body.newPassword;

	User.findOne({email: user})
	.then(result => {
		if(result === null){
			return response.send(false)
		}else{
			const isPasswordCorrect = bcrypt.compareSync(oldPass, result.password)

			if(isPasswordCorrect){
				User.updateOne({email: user},{password: newPass},{new: true})
				.then(result => result)
			}else{
				return response.send(false)
			}
		}
	})
	.catch(error => response.send(false))
}