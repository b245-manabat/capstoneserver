const Order = require("../Models/ordersSchema.js");
const Product = require("../Models/productsSchema.js");
const Coupon = require("../Models/couponsSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");


module.exports.addToCart = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const user = userData._id;
	const input = request.body;
	const _id = request.params.id;

	let isAvailable = await Product.findById({_id: _id})
	.then(result => {if(result.stocks <= 0 || result.stocks < input.quantity){return false}else{return true}})
	.catch(error => response.send(false))

	
	if(!userData.isAdmin && isAvailable) {

		let userList = await Order.findOne({userId: user, purchasedOn: null})
		.then(result => {
			if(result === null) {return false}
			else {return true}
			
		})
		.catch(error => false)


		if(!userList) {
			await Product.findOne({_id: _id})
			.then(result => {
				let newOrder = new Order({
					userId : user,
					products : {
						productId:_id,
						quantity:input.quantity
					}
				})

				newOrder.save()
				.then(save => response.send(save))
				.catch(error => response.send(false));
			})
			.catch(error => response.send(false));
		}
		else {
			let isItemExisting = await Order.findOne({userId:user, purchasedOn: null})
			.then(result => {
				let prods = result.products;
				let found = prods.some(
					function(prods) {
						if (prods.productId == _id) {
							return true
						}
						else {
							return false
						}
					}
				);
				return found
			})
			.catch(error => response.send(false));

			if(!isItemExisting) {

				Order.findOne({userId: user, purchasedOn: null})
					.then(result =>{
						result.products.push({
								productId: _id,
								quantity: input.quantity
							})
						return result.save()
						.then(result => response.send(result))
						.catch(error => response.send(false))
					})
					.catch(error => response.send(false))
				}
			else {
				if(input.quantity <= 0) {
					Order.findOne({userId:user, purchasedOn: null})
					.then(result => {
						let productList = result.products;
						for(let i=0;i<productList.length;i++){
							if(productList[i].productId == _id){
							productList.splice(i,1);
							break;
							}
						}

						Order.findOneAndUpdate({
							"userId":user,
							"purchasedOn" : null
						},
						{
							$set: {
								"products": productList
							}
						},
						{new:true})
						.then(result => response.send(result))
						.catch(error => response.send(false))
					})
					.catch(error => false)
				}
				else {

					 Order.findOneAndUpdate({
					 	"userId":user,
					 	"products.productId":_id,
					 	"purchasedOn": null
					 },
					 {
					 	$set: {
					 		"products.$.quantity": input.quantity
					 	}
					 },
					 {new:true})
					.then(result => response.send(result))
					.catch(error => response.send(false))
				}

			}

		}
	}
	else {
		return response.send(false)
	}
}


module.exports.viewOrder = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const user = userData._id;
	const input = request.body;

	let order = await Order.findOne({"userId":user, "purchasedOn": null	}).then(order => order);

	if(order !== null){
		let products = await Product.find({}).then(prods => prods);
		let orderList = order.products;
		let items = [];

		let discount = await Coupon.findOne({"couponCode": input.couponCode})
		.then(result => {
			if(result == null) {
				return 0;
			}
			else if(result.couponCode == input.couponCode && result.isActive){
				return result.couponDiscount;
			}
			else {
				return 0;
			}
		})
		.catch(error => false);
		let totalAmount = 0 - discount;
		
		for(let i=0;i<orderList.length;i++){
			let eachItem = orderList[i].productId;
			let itemQuantity = orderList[i].quantity;
			let srp = 0;

			for(let x=0;x<products.length;x++){
				if(products[x]._id == eachItem){
					srp = products[x].price-products[x].itemDiscount
				}
			}

			let subTotal = itemQuantity*srp;
			totalAmount += subTotal;
			await Order.updateOne({"userId":user,"products.productId":eachItem,"purchasedOn": null},{$set:{"products.$.subTotal":subTotal}},{new:true}).then(result=>result)
		}

		Order.findOneAndUpdate({"userId":user,"purchasedOn": null},{$set: {"name":input.name,"address":input.address,"contact":input.contact,"couponCode":input.couponCode,"paymentMethod":input.paymentMethod,"totalAmount": totalAmount,"purchasedOn": input.purchasedOn,"isPurchased":input.isPurchased}},{new:true})
		.then(listUpdated => {


			// if(input.purchasedOn!==null){
			// 	for(let i=0;i<orderList.length;i++){
			// 		let eachItem = orderList[i].productId;
			// 		let itemQuantity = orderList[i].quantity;
					
			// 		Product.findByIdAndUpdate({eachItem},{$set:{"stocks": result.stocks-itemQuantity,"itemsSold": result.itemsSold+itemQuantity}},{new:true}).then(result => listUpdated)

			// 		// await Product.updateById({eachItem},{"stocks": item.stocks-itemQuantity,"itemsSold": itemsSold+itemQuantity},{new:true})
			// 		// .then(itemUpdated => {
			// 		// 	console.log(itemUpdated)
			// 		// 	return response.send(itemUpdated)})
			// 		// .catch(error => false)
			// 		// // await Product.findByIdAndUpdate({eachItem},{$set: {"stocks": {$subtract: ["$stocks","$itemQuantity"]},"itemsSold": {$add: ["$itemsSold","$itemQuantity"]}}},{new:true}).then(itemUpdated => itemUpdated).catch(error => false)
			// 	}
			// }
				return response.send(listUpdated)

		})
		.catch(error => response.send(false));
	} else {
		return false
	}
}

module.exports.allOrder = (request, response) => {
	const userData = auth.decode(request.headers.authorization);


	if(userData.isAdmin) {
		Order.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		return response.send(false);
	}
}

module.exports.isPurchased = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const user = userData._id;

	if(userData.isAdmin) {
		Order.find({"isPurchased": true, "isDelivered": false})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		Order.find({"userId":user, "isPurchased": true, "isDelivered": false})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
}

module.exports.isDelivered = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const user = userData._id;

	if(userData.isAdmin) {
		Order.find({"isDelivered": true,"isReceived": false})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		Order.find({"userId":user,"isDelivered": true,"isReceived": false})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
}

module.exports.isReceived = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const user = userData._id;

	if(userData.isAdmin) {
		Order.find({"isReceived": true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		Order.find({"userId":user,"isReceived": true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
}

module.exports.isCancelled = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const user = userData._id;

	if(userData.isAdmin) {
		Order.find({"isCancelled": true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
	else {
		Order.find({"userId":user,"isCancelled": true})
		.then(result => response.send(result))
		.catch(error => response.send(false))
	}
}
